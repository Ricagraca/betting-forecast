mkdir data
url=https://www.football-data.co.uk
seasons="0001 0102 0203 0304 0405 0506 0607 0708 
	 0809 0910 1011 1112 1213 1314 1415 1516
	 1617 1718 1819"
folder=mmz4281
division=E0

download()
{
	# SEASON
	S=$1
	# FOLDER
	F=$2
	# DIVISION
	D=$3
	curl $url/$F/$S/$D.csv > data/$D$S.csv
}

for season in $seasons; do 
	download $season $folder $division
done
