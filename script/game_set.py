import game
import csv
import game as G
import elo_model as E


class GameSet():

      def __init__(self, file_names = None):

      	  # CREATE A VECTOR OF GAMES
      	  self.GAME = []

          # ELO MODEL
          self.elo = E.Elo()

          if file_names <> None:
                for file in file_names:
                      self.__read__(file)
                      
          
      def __read__(self, filename):
            
            # Open csv file
            csv_file = open(filename)

            # Create a reader for the csv file
            csv_dict = csv.DictReader(csv_file, delimiter=',')

            # Create an object with every dictionary
            current = self.GAME
            new_games  = [G.Game(game_dict) for game_dict in csv_dict]
            
            self.GAME = new_games + current

            
      def read_teams(self, new_games):

            T = set({})
            
            # START READING NAME OF TEAMS
            for game in new_games:
                  T.add(game.home_team)
                  T.add(game.away_team)                  

            return T

            
      def update_ratings(self, ki=14):
            
            # ELO MODEL
            self.elo = E.Elo(k=ki)
          
            for game in self.GAME:
                  self.elo.update_on_game(game)
      
                  
      def get_ratings(self):
            return self.elo.R
            

      # Accuracy in test data over only H A (No draws
      def accuracy(self, test_set):

            data_set = test_set.GAME
            R = self.get_ratings()
            
            filter_func = lambda game : game.full_time_result <> 'D' \
                  and game.home_team in R \
                  and game.away_team in R
            
            data_set = filter(filter_func, data_set) # Number of games
            N = len(data_set)
            C = 0 # Model got C correct
            
            for game in data_set:
                  result = game.full_time_result
                  home_team = game.home_team
                  away_team = game.away_team
                  game_predict = 'H' \
                        if self.elo.predict(home_team, away_team) > 0  \
                           else \
                                'A'      

                  if home_team not in R or away_team not in R:
                        raise Exception()
                  if result == game_predict:
                        C += 1

            return float(C) / float(N)


      # Quadratic loss
      def loss(self, test_set):

            data_set = test_set.GAME
            R = self.get_ratings()
            
            filter_func = lambda game : game.full_time_result <> 'D' \
                  and game.home_team in R \
                  and game.away_team in R
            
            data_set = filter(filter_func, data_set) # Number of games
            L = 0 # Model got C correct
            
            for game in data_set:
                  result = game.full_time_result
                  home_team = game.home_team
                  away_team = game.away_team
                  game_predict = self.elo.predict(home_team, away_team)
                  L += abs((game_predict - self.elo.actual(result)))**2

            return L
