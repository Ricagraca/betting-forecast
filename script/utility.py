
def option(dict, key):
    if key in dict:
        return dict[key]
    else:
        return None
