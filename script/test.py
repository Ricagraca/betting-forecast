import csv
import sys
import game_set as GS
import matplotlib.pyplot as plt

def acc(ki):

    # Insert a file name
    # Training files
    filetrain = sys.argv[1].split(';')

    Training = GS.GameSet(filetrain)

    # Update rankings on training test
    Training.update_ratings(ki=k)

    # Test files
    filetest = sys.argv[2].split(';')

    Test = GS.GameSet(filetest)

    # Return accuracy
    return Training.accuracy(Test)
    

if __name__ == "__main__":

    # Print the accuracy on test data
    c = 10
    x = [float(i)/float(c) for i in range(5*c,26*c)]
    y = [acc(float(k)/float(c)) for k in range(5*c,26*c)]
         
    plt.plot(x, y)
    plt.show()
