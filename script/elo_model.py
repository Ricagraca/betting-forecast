import math

class Elo():
    
    def __init__(self, k=4, l=0):

        # Ratings 
        self.R = {}

        # Some parameters are hard coded
        self.c = 10
        self.d = 400

        # Essential parameters
        self.k = k
        self.lamb = l

        # Every team starts off with the same rating
        self.starting_rating = 1000
        

    # w is the measure of home advantage
    def expected(self, H, A, w=80):

        exponent = (A - H - w)/float(self.d)
        denominator = 1.0 + (self.c ** exponent)
        return 1.0/denominator


    def actual(self, result):
        if result == 'H':
            return 1.0
        if result == 'A':
            return 0.0
        return 0.5


    def predict(self, home, away):
        return 1.0 if self.R[home] - self.R[away] > 0 else 0.0

    
    # Update rating stats on game
    def update_on_game(self, game):

        home_team = game.home_team
        away_team = game.away_team
        result = game.full_time_result

        home_rating = self.starting_rating if home_team not in self.R else self.R[home_team]
        away_rating = self.starting_rating if away_team not in self.R else self.R[away_team]

        home_expected_result = self.expected(home_rating,away_rating)
        # away_expected_result = 1.0 - home_expected_result

        home_actual_result = self.actual(result)
        # away_actual_result = 1.0 - home_actual_result
        
        delta_home = home_actual_result - home_expected_result
        delta_away = -delta_home

        if game.full_time_home_goals == ' ':
            goal_difference = int(game.full_time_home_goals) - int(game.full_time_away_goals)
            goal_difference = abs(goal_difference)
        else:
            goal_difference = 0
            
        k = self.k * (1 + goal_difference) ** self.lamb
        
        self.R[home_team] = home_rating + k * (delta_home)
        self.R[away_team] = away_rating + k * (delta_away)        


        
    
