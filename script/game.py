from utility import option

'''
'''

class Game():

    def read_bets(self):

        game_dict = self.game_dict

        # Betting odds data
        self.bet365_home_win_odds = option(game_dict,'B365H')
        self.bet365_draw_odds = option(game_dict,'B365D')
        self.bet365_away_win_odds = option(game_dict,'B365A')
        
        self.blue_square_home_win_odds = option(game_dict,'BSH')
        self.blue_square_draw_odds = option(game_dict,'BSD')
        self.blue_square_away_win_odds = option(game_dict,'BSA')
        
        self.bet_and_win_home_win_odds = option(game_dict,'BWH')
        self.bet_and_win_draw_odds = option(game_dict,'BWD')
        self.bet_and_win_away_win_odds = option(game_dict,'BWA')
        
        self.gamebookers_home_win_odds = option(game_dict, 'GBH')
        self.gamebookers_draw_odds = option(game_dict,'GBD')
        self.gamebookers_away_win_odds = option(game_dict,'GBA')
        
        self.interwetten_home_win_odds = option(game_dict,'IWH')
        self.interwetten_draw_odds = option(game_dict,'IWD')
        self.interwetten_away_win_odds = option(game_dict,'IWA')
        
        self.ladbrokes_home_win_odds = option(game_dict,'LBH')
        self.ladbrokes_draw_odds = option(game_dict,'LBD')
        self.ladbrokes_away_win_odds = option(game_dict,'LBA')
        
        self.pinnacle_home_win_odds = (option(game_dict,'PSH'), option(game_dict,'PH'))
        self.pinnacle_draw_odds = (option(game_dict,'PSD'), option(game_dict,'PD'))
        self.pinnacle_away_win_odds = (option(game_dict,'PSA'), option(game_dict,'PA'))
        
        self.sporting_odds_home_win_odds = option(game_dict,'SOH')
        self.sporting_odds_draw_odds = option(game_dict,'SOD')
        self.sporting_odds_away_win_odds = option(game_dict,'SOA')

        self.sportingbet_home_win_odds = option(game_dict,'SBH')
        self.sportingbet_draw_odds = option(game_dict,'SBD')
        self.sportingbet_away_win_odds = option(game_dict,'SBA')

        self.stan_james_home_win_odds = option(game_dict,'SJH')
        self.stan_james_draw_odds = option(game_dict,'SJD')
        self.stan_james_away_win_odds = option(game_dict,'SJA')
        
        self.stanleybet_home_win_odds = option(game_dict,'SYH')
        self.stanleybet_draw_odds = option(game_dict,'SYD')
        self.stanleybet_away_win_odds = option(game_dict,'SYA')
        
        self.vc_bet_home_win_odds = option(game_dict,'VCH')
        self.vc_bet_draw_odds = option(game_dict,'VCD')
        self.vc_bet_away_win_odds = option(game_dict,'VCA')
        
        self.william_hill_home_win_odds = option(game_dict,'WHH')
        self.william_hill_draw_odds = option(game_dict,'WHD')
        self.william_hill_away_win_odds = option(game_dict,'WHA')


    def read_statistics(self):

        game_dict = self.game_dict

        # Match statistics
        self.referee = game_dict['Referee']
        self.home_team_shots = game_dict['HS']
        self.away_team_shots = game_dict['AS']
        self.home_team_shots_on_target = game_dict['HST']
        self.away_team_shots_on_target = game_dict['AST']
        self.home_team_corner = game_dict['HC']
        self.away_team_corner = game_dict['AC']
        self.home_team_yellow_cards = game_dict['HY']
        self.away_team_yellow_cards = game_dict['AY']
        self.home_team_red_cards = game_dict['AR']
        self.away_team_red_cards = game_dict['AR']


    def read_information(self):

        game_dict = self.game_dict
        
        # Which division is the game
        self.division = game_dict['Div']

        # Time and Date that the game occured
        self.date = game_dict['Date']

        # Team who plays the game
        self.home_team = game_dict['HomeTeam']
        self.away_team = game_dict['AwayTeam']

        # Information about the goals in game
        self.full_time_home_goals = game_dict['FTHG']
        self.full_time_away_goals = game_dict['FTAG']
        self.full_time_result = game_dict['FTR']
        self.half_time_home_goals = game_dict['HTHG']
        self.half_time_away_goals = game_dict['HTAG']
        
    
    def __init__(self, game_dict):

        # Store game dictionary
        self.game_dict = game_dict

        self.read_information()
        self.read_statistics()
        self.read_bets()

        
    def winning_team(self):
        if   self.full_time_result == 'H':
            return self.home_team
        elif self.full_time_result == 'A':
            return self.away_team
        elif self.full_time_result == 'D':
            return ""
